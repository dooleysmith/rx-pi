import time

import time
import json
import requests
import unicornhat as uh

uh.set_layout(uh.PHAT)
uh.brightness(0.5)

def set_blue():
    for x in range(8):
        for y in range(4):
            uh.set_pixel(x, y, 255, 0, 0)
    print('error settings lights - blue')

def set_green():
    for x in range(8):
        for y in range(4):
            uh.set_pixel(x, y, 0, 255, 0)
    print('status free - setting LEDs green')

def set_amber():
    for x in range(8):
        for y in range(4):
            uh.set_pixel(x, y, 255, 126, 0)
    print('status checkin - setting LEDs amber')

def set_red():
    for x in range(8):
        for y in range(4):
            uh.set_pixel(x, y, 255, 0, 0)
    print('status busy - setting LEDs red')

def get_status():
    url = "http://192.168.0.37/api/Bookings/Getdata"
    payload = "{'resid': 1, 'requestdate': ''}"
    headers = {
        'ApiKey': 'secretBatCode1234',
        'Content-Type': 'application/json',
        'ScreenType': 'Unicorn',
        'Referer': 'pi.zero'
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    data = response.json()
    return data['d']['Resource']['Status']


while True:
    status = get_status()
    print(status)

    if status == 1:
        set_green()

    if status == 2:
        set_amber()

    if status == 3:
        set_red()

    if status == 4:
        set_red()

    uh.show()
    time.sleep(10)